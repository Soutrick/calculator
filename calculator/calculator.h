#ifndef CALCULATOR_H
#define CALCULATOR_H

void initCalculator();

int check_expression();

void add_to_queue(int value);

void delete_from_queue();

#endif /* CALCULATOR_H */