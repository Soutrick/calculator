#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/queue.h>
#include "calculator.h"

typedef struct QueueElement {
    int value;
    SLIST_ENTRY(QueueElement) pointers;
} QueueElement;

SLIST_HEAD( , QueueElement) head = SLIST_HEAD_INITIALIZER(&head);

/**
 * @brief USed to intialise SLIST
 *
 */
void
initCalculator()
{
    SLIST_INIT(&head);
}


/**
 * @brief Used to add value to the SLIST
 *
 * @param value
 */
void
enqueue(int value)
{
    QueueElement *new_element = malloc(sizeof(struct QueueElement));

    if (!new_element) {
        printf("Memory allocation failed\n");
        exit(EXIT_FAILURE);
    }
    new_element->value = value;
    SLIST_INSERT_HEAD(&head, new_element, pointers);
}


/**
 * @brief Used to evaluate the expression and produce the final result
 *
 * @return int
 */
int
check_expression()
{
    char          op;
    QueueElement *current_element;
    int           result = 0;

    SLIST_FOREACH(current_element, &head, pointers)
    {
        // printf("Value_1: %d \n", (current_element->value));
        if ((char)current_element->value != '+' &&
            (char)current_element->value != '-') {
            // printf("Value: %d \n", current_element->value);
            switch (op) {
            case '+':
                result += current_element->value;
                break;

            case '-':
                result += current_element->value;
                break;

            default:
                result = current_element->value;
            }
        }else {
            op = (char)current_element->value;
            // printf("Operand: %c \n", op);
        }
    }

    return (result);
}


/**
 * @brief Used to add value to the SLIST
 *
 * @param value
 */
void
add_to_queue(int value)
{
    enqueue(value);
}


/**
 * @brief
 *
 */
void
delete_from_queue()
{
    if (SLIST_EMPTY(&head)) {
        printf("Calculator is Empty, nothing to delete");
        return;
    }
    // Deleteing value
    SLIST_REMOVE_HEAD(&head, pointers);
}