# Usage:
# make			# compile all .c files to a single binary output
# make clean	# remove ALL binaries and objects 	 

.PHONY = all clean_log build run clean
all: clean build run 

CC = gcc							# compiler to use
OUTPUT_NAME = main					# name of the output file

SRCS=main.c calculator/calculator.c
OBJS=$(SRCS:.c=.o)


clean_log:
	@echo Cleaning Log File$(NC);
	@rm output.log || true

build:
	@echo Compiling all c files$(NC);
	@$(CC) $(SRCS) -o $(OUTPUT_NAME)

run:
	@echo Running executable$(NC);
	@./$(OUTPUT_NAME)

clean:
	@echo Cleaning executable file$(NC);
	@rm $(OUTPUT_NAME)