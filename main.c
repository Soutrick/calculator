#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include "calculator/calculator.h"

/**
 * @brief
 *
 * @return int
 */
int
main()
{
    // To initialize the calculator
    initCalculator();
    char op;
    int  value;

    printf("Enter the first number: ");
    scanf("%d", &value);
    // printf("value: %d", value);
    add_to_queue(value);


    while (true) {
        printf("Enter the operator (+ or -) or  (Press 'd' to delete, 'q' to quit): \n");
        scanf(" %c", &op);
        if (op == 'q') {
            break;
        }else if (op == 'd') {
            delete_from_queue();
            continue;
        }
        add_to_queue(op);
        printf("Enter the next number: ");
        scanf("%d", &value);
        // printf("value: %d", value);
        add_to_queue((op == '-')? -value : value);
        // print_queue();
        int result = check_expression();
        printf("Result: %d\n", result);     // Print the result
    }
    return (0);
}